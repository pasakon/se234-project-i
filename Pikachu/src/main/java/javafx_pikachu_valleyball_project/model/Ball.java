package javafx_pikachu_valleyball_project.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx_pikachu_valleyball_project.view.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Ball extends Pane {

    private static final Logger logger = LogManager.getLogger(Character.class.getName());

    private Platform platform;
    private AnimatedSprite imageView;
    private Image ballImg;
    private static final int WIDTH = 92,HEIGHT = 92;
    private static float GRAVITY = .98f;

    private int x,y;
    private float smash_power,power,px,py;
    private int max_yDistance;
    private boolean is_falling;
    private boolean is_wall;
    private KeyCode slowBall;
    private KeyCode speedBall;

    private static final int[][] respawn_point = {{150,100},{Platform.WIDTH-150,100}};

    public Ball(Platform platform, int x, int y, float power, float smash_power, KeyCode slowBall, KeyCode speedBall){
        this.platform = platform;
        this.x = x;
        this.y = y;
        this.setTranslateX(this.x);
        this.setTranslateY(this.y);
        this.ballImg = new Image("/assets/ball_sprite.png");
        this.imageView = new AnimatedSprite(ballImg,5,5,0,0,42,40);
        this.imageView.setFitWidth(WIDTH);
        this.imageView.setFitHeight(HEIGHT);
        this.power = power;
        this.smash_power = smash_power;
        this.slowBall = slowBall;
        this.speedBall = speedBall;
        is_falling = true;
        is_wall = false;
        getChildren().add(imageView);
    }

    public  void removeBall(){
        is_falling =false ;
        is_wall = false;
        x = -1000;
        y= -1000;

    }
    public void checkReachFloor(ArrayList<Character> c) {
        if(y >= Platform.GROUND - HEIGHT) {
            y = Platform.GROUND - HEIGHT;
            if (x<Platform.WIDTH/2){
                javafx.application.Platform.runLater(()->{
                    c.get(1).setScore(c.get(1).getScore()+1);
                    platform.getScoreList().get(1).setScore(c.get(1).getScore());
                    respawn(respawn_point[0][0],respawn_point[0][1]);
                });
            }else{
                javafx.application.Platform.runLater(()->{
                    c.get(0).setScore(c.get(0).getScore()+1);
                    platform.getScoreList().get(0).setScore(c.get(0).getScore());
                    respawn(respawn_point[1][0],respawn_point[1][1]);
                });
            }
        }

    }

    public void check_hit_character(Character character,Platform platform){
        if(getBoundsInParent().intersects(character.getBoundsInParent())) {
                
            float p =  platform.getKeys().isPressed(character.getSpecialKey())&&!character.canJump?smash_power:power;  ;

            if(platform.getKeys().isPressed(character.getSpecialKey2())){
                p= platform.getKeys().isPressed(character.getSpecialKey2())&&!character.canJump?smash_power-1000:power;
            }
            if(platform.getKeys().isPressed(character.getSpecialKey())){
                p= platform.getKeys().isPressed(character.getSpecialKey())&&!character.canJump?smash_power:power;
            }
                float cx = (float)character.getBoundsInParent().getCenterX();
                float cy = (float)character.getBoundsInParent().getCenterY();
                float bx = (float)getBoundsInParent().getCenterX();
                float by = (float)getBoundsInParent().getCenterY();
                float h = Math.abs((float) Math.sqrt(Math.pow(bx-cx,2)+Math.pow(by-cy,2)));
                float a = Math.abs(bx-cx);
                float o = Math.abs(by-cy);
                if (bx-cx<0){
                    px = -(p * a / h);
                    py = p * o/h;
                }else{
                    px = p * a / h;
                    py = p * o/h;
                }
                py = py<8?8:py;
                max_yDistance = (int) (getY()-(Math.abs((Math.pow(py,2)/(2*GRAVITY)))));
                is_falling = false;
                is_wall = true;

        }

        }


    public void check_hit_counter(Character character,Platform platform){
        float p = platform.getKeys().isPressed(character.getSpecialKey())&&!character.canJump?smash_power:power;
        if (getBoundsInParent().intersects(character.getBoundsInParent())&& p==smash_power){
            if (platform.getKeys().isPressed(character.getCounter())){
                if (x<Platform.WIDTH/2){
                    javafx.application.Platform.runLater(()->{respawn(respawn_point[1][0],respawn_point[1][1]);});
                }else{
                    javafx.application.Platform.runLater(()->{respawn(respawn_point[0][0],respawn_point[0][1]);});
                }
            }
        }
    }
    public void checkHitWall(Wall wall){
        if (getX()<0||getX()+WIDTH>=Platform.WIDTH){
            px *= -1;
        }
        if(getBoundsInParent().intersects(wall.getBoundsInParent())&&is_wall){
            px*=-1;
            is_wall = false;
        }
        if(getY()<=0){
            is_falling = true;
        }
    }

    public void checkReachHeight(){
        if(py<=0){
            py = 0;
            is_falling = true;
        }
    }

    public void moveX(){
        setTranslateX(x);
        x = (int)(x+px);
    }

    public void moveY(){
        checkReachHeight();
        setTranslateY(y);
        if(is_falling){
            py += GRAVITY/10;
            y+=py;
        }else{
            y-=py;
            py-=GRAVITY;
        }
    }

    public void repaint(){
        imageView.ball_anim();
        moveX();
        moveY();
    }

    private void respawn(int x,int y){
        this.x = x;
        this.y = y;
        is_falling = true;
        is_wall = false;
        px = 0;
        py = 0;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    public KeyCode getSlowBall() {
        return slowBall;
    }

    public KeyCode getSpeedBall(){
        return speedBall;
    }

    public void trace(Keys keys){
        String action = "";
        if(keys.isPressed(slowBall) ==true){
            action = "Slow";
        }
        logger.debug("Action:"+action);
    }

    public void slowBall(){
        new Thread(()->{
            long time = System.currentTimeMillis();
            //long threeSecond = time + 5000;
            long threeSecond = time + 3000;
            while(System.currentTimeMillis() < threeSecond){
                is_falling = false;
            }

        }).start();
    }

    public void speedBall(){
        new Thread(()->{
            long time = System.currentTimeMillis();
            //long threeSecond = time + 5000;
            long threeSecond = time + 3000;
            while(System.currentTimeMillis() < threeSecond){
                GRAVITY = 10f;
            }
            GRAVITY = .98f;

        }).start();
    }
}
